"""excercise1 Write a program that reads the words in words.txt and stores them as
keys in a dictionary. It doesn’t matter what the values are. Then you
can use the in operator as a fast way to check whether a string is in the dictionary"""

fhand = open('words.txt', 'r')
#print(fhand.read())
counts = dict()
for line in fhand:
    words = line.split()
    for word in words:
        if word not in counts:
            counts[word] = 1
        else:
            counts[word] += 1
#print(counts)
print('creative' in counts)



"""exercise 2 Write a program that categorizes each mail message by
which day of the week the commit was done. To do this look for lines
that start with “From”, then look for the third word and keep a running
count of each of the days of the week. At the end of the program print
out the contents of your dictionary (order does not matter).
"""
fileHand = open('mbox-short.txt', 'r')
#print(fileHand.read())
weekdays = dict()
for line in fileHand:
    if line.startswith('From '):
       words = line.split()
       #print(words)
       if words[2] not in weekdays:
           weekdays[words[2]] = 1
       else:
           weekdays[words[2]] += 1
print(weekdays)

""" Exercise 3: Write a program to read through a mail log, build a histogram using a dictionary to count how many messages have come from
each email address, and print the dictionary.  
    Exercise 4: Add code to the above program to figure out who has the
most messages in the file. After all the data has been read and the dictionary has been created, look through the dictionary using a maximum
loop (see Chapter 5: Maximum and minimum loops) to find who has
the most messages and print how many messages the person has.
"""

fileHand = open('mbox-short.txt', 'r')
#print(fileHand.read())
try:
    Hillary = dict()
    largest = None
    spammed = ""
    for line in fileHand:
        if line.startswith('From: '):
            words = line.split()
       #print(words)
            if words[1] not in Hillary:
                Hillary[words[1]] = 1
            else:
                Hillary[words[1]] += 1
    for name, i in Hillary.items():
        if largest is None or i > largest:
            largest = i
            spammed = name
    print('The person spamming hardest is', spammed, 'with a whopping', largest, "mails")
except FileNotFoundError:
    print('Yo where my file bitch?')











